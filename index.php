<?php

/**
 * Notes: 
 * 1) To calculate the overhead I used progressive substraction using
 *      memory_get_peak_usage(true) and memory_get_usage(true);
 * 2) Tested with files up to 20 GB with memory_limit up to 1GB
 * 3) Tested on Linux with PHP 8.1 running on a Docker with this image: php:8.1.0alpha3-apache 
 * 
 */


//setting the memory limit to 16 MB
ini_set('memory_limit', '16M');

/** @var string $sFileNameA representing the path to file A*/
$sFileNameA = '1g.img';
/** @var string $sFileNameB representing the path to file B*/
$sFileNameB = '2g.img';

/** @var bool|null the result of the compare_files */
$result = compare_files($sFileNameA, $sFileNameB);

//to test the program, I'll use var_dump to print the type and content of the $result
var_dump($result);


/**
 * Compare the content of the given paths to files to find out if the content is the same
 * 
 * @param string $sFileNameA the path to the file A to compare
 * @param string $sFileNameB the path to the file B to compare
 * 
 * @return bool|null true means the content of both files is the same;
 *                   false means that the content of the files differs;
 *                   null means that either of them isn't a file, isn't readable
 *                        or that some problem has occured during the comparison
 */
function compare_files(string $sFileNameA, string $sFileNameB): ?bool
{
    try {

        //checks if both given strings are actually files and readable
        if (
            is_readable($sFileNameA) === false || is_readable($sFileNameB) === false
            || is_file($sFileNameA) === false || is_file($sFileNameB) === false
        ) {
            return null;
        }

        /**
         * Leaving the next two controls commented, as their presence and results might be contextual
         */
        //checks if the given strings point towards the same file are the same file
        /*
        if ($sFileNameA === $sFileNameB) {
            //return null;
            //return true;
        }

        //checks if the file has a size
        if (filesize($sFileNameA) === 0){
            return null;
        }
        */

        //checks if both files have the same size
        if (filesize($sFileNameA) !== filesize($sFileNameB)) {
            return false;
        }

        /**
         * I personally would prefer to avoid this method, since there's a chance,
         * as ridiculously low as it might be, that 2 different files have the same hash result
         */
        //checks if the hash of the files differs
        /*
        if (hash_file('xxh3', $sFileNameA) !== hash_file('xxh3', $sFileNameB)){
            return false;
        }
        */


        /** 
         * The mode used to open the files:
         * r to specify that we'll only read them and b to forces binary mode
         */

        /** @var resource $resourceA the resource corresponding to file A */
        $resourceA = fopen($sFileNameA, 'rb');
        
        //checks if file A failed to be opened
        if ($resourceA === false){
            return null;
        }

        //checks if file A didn't locked successfully
        if (flock($resourceA, LOCK_SH) === false){
            //closes file A then returns null
            fclose($resourceA);
            return null;
        }

        /** @var resource $resourceB the resource corresponding to file B */
        $resourceB = fopen($sFileNameB, 'rb');

        //checks if file B failed to be opened
        if ($resourceB === false) {
            //unlock and closes file A then returns null
            flock($resourceA, LOCK_UN);
            fclose($resourceA);
            return null;
        }

        //checks if file B didn't locked successfully
        if (flock($resourceB, LOCK_SH) === false){
            //unlocks file A, closes both files and returns null
            flock($resourceA, LOCK_UN);
            fclose($resourceA);
            fclose($resourceB);
            return null;
        }
        /**
         * Since we have 16M limit, the max bytes we can read is:
         *  + memory allowed of 16 MB (16*1024*1024)                            + 16'777'216
         *  - memory currently used by php (in this example is 2097152)         -  2'097'152
         * __________________________________________________________________________________
         *  = total max memory allocable to read                                = 14'680'064
         *  dividing by 3 (because when overwriting the variables                        / 3
         *  $analyzedBytes#, it temporary stores both the previous and current values)
         * _________________________________________________________________________________
         *  floored value, to avoid decimals                            (floor) =  4'893'354
         *  - max overhead expected (it's contextual, found out via             -       2755
         *   empirical method: setting it to 2754 would cause a memory leak
         * _________________________________________________________________________________
         *  = single max memory allocable to read                               =  4'890'599
         */

        /** @var int $length the number of bytes to compare each loop */
        $length = (int) floor((16 * 1024 * 1024 - memory_get_usage(true)) / 3) - 2755;

        //loops through the files until both of them reach their end
        while (!feof($resourceA) && !feof($resourceB)) {

            /** @var string|false $analyzedBytesA the string to analyze from the first file */
            $analyzedBytesA = fread($resourceA, $length);

            /** @var string|false $analyzedBytesA the string to analyze from the second file */
            $analyzedBytesB = fread($resourceB, $length);

            //checks if the read string differ in length and neither file reached their end
            if (
                strlen($analyzedBytesA) !== strlen($analyzedBytesB)
                && feof($resourceA) === false && feof($resourceB) === false
            ) {
                //if that's the case, then unlocks and closes both file before returning null
                flock($resourceA, LOCK_UN);
                flock($resourceB, LOCK_UN);
                fclose($resourceA);
                fclose($resourceB);
                return null;
            }

            /**
             * Checks if either of the File A or B reached its end but not both,
             * or if their contents differ from eachother
             */
            if ((feof($resourceA) === true xor feof($resourceB) === true)
                || $analyzedBytesA !== $analyzedBytesB
            ) {
                //if that's the case, then unlocks and closes both file before returning false
                flock($resourceA, LOCK_UN);
                flock($resourceB, LOCK_UN);
                fclose($resourceA);
                fclose($resourceB);
                return false;
            }
        }
        /**
         * If the functions reached this point,
         * it means that every byte of the given files corresponds with one another
         * hence closes both file and returns true
         */
        flock($resourceA, LOCK_UN);
        flock($resourceB, LOCK_UN);
        fclose($resourceA);
        fclose($resourceB);
        return true;
    } catch (Exception $e) {
        //leaving the var_dump commented so it can be quickly uncommented to help debugging
        //var_dump($e);
        //in case of an Exception (which anyway shouldn't happen) returns null
        return null;
    }
}
